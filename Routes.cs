﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Mvc.Routes;

namespace Proligence.Collections {
    
    public class Routes : IRouteProvider {
        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (RouteDescriptor routeDescriptor in GetRoutes()) {
                routes.Add(routeDescriptor);
            }
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Priority = 25,
                    Route = new Route(
                        "Admin/Common/Collection/Add",
                        new RouteValueDictionary {
                            {"area", "Proligence.Collections"},
                            {"controller", "Admin"},
                            {"action", "Add"},
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Proligence.Collections"}
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 25,
                    Route = new Route(
                        "Admin/Common/Collection/Remove",
                        new RouteValueDictionary {
                            {"area", "Proligence.Collections"},
                            {"controller", "Admin"},
                            {"action", "Remove"},
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Proligence.Collections"}
                        },
                        new MvcRouteHandler())
                },
                                new RouteDescriptor {
                    Priority = 25,
                    Route = new Route(
                        "Admin/Common/Collection/Move",
                        new RouteValueDictionary {
                            {"area", "Proligence.Collections"},
                            {"controller", "Admin"},
                            {"action", "Move"},
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Proligence.Collections"}
                        },
                        new MvcRouteHandler())
                }  
            };
        }
    }
}