using System;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Environment.Configuration;
using Orchard.Environment.Features;
using Orchard.Logging;

namespace Proligence.Collections
{
    public class Migrations : DataMigrationImpl
    {
        private readonly IFeatureManager _featureManager;
        private readonly ShellSettings _shellSettings;

        public Migrations(IFeatureManager featureManager, ShellSettings shellSettings)
        {
            _featureManager = featureManager;
            _shellSettings = shellSettings;
        }

        public ILogger Logger { get; set; }

        public int Create()
        {

            try
            {
                RenameTable(
                    "Proligence_Common_CollectionPartRecord",
                    "Proligence_Collections_CollectionPartRecord"
                );
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Cannot rename old CollectionPartRecord table. Creating new, empty one instead.");
                SchemaBuilder.CreateTable("CollectionPartRecord", table => table
                    .ContentPartRecord());
            }

            try
            {
                RenameTable(
                    "Proligence_Common_CollectionRelationRecord",
                    "Proligence_Collections_CollectionRelationRecord"
                );
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Cannot rename old CollectionRelationRecord table. Creating new, empty one instead.");
                SchemaBuilder.CreateTable("CollectionRelationRecord", table => table
                    .Column<int>("Id", c => c.PrimaryKey().Identity())
                    .Column<int>("CollectionPartRecord_Id")
                    .Column<int>("ContentItemRecord_Id")
                    .Column<string>("Position"));
            }

            ContentDefinitionManager.AlterPartDefinition("CollectiblePart", p => p.Attachable());
            ContentDefinitionManager.AlterPartDefinition("CollectionPart", p => p.Attachable());

            return 1;
        }

        private void RenameTable(string oldName, string newName)
        {
            SchemaBuilder.ExecuteSql(string.Format("sp_rename '{0}', '{1}'", PrefixTableName(oldName), PrefixTableName(newName)),
                sql => sql
                    .ForProvider("SqlCe")
                    .ForProvider("SqlServer"));

            SchemaBuilder.ExecuteSql(string.Format("rename table `{0}` to `{1}`", PrefixTableName(oldName), PrefixTableName(newName)),
                sql => sql
                    .ForProvider("MySql"));
        }

        private string PrefixTableName(string tableName)
        {
            if (string.IsNullOrEmpty(_shellSettings.DataTablePrefix))
                return tableName;
            return _shellSettings.DataTablePrefix + "_" + tableName;
        }
    }
}