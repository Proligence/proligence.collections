﻿using System;
using System.Web.Mvc;
using Orchard;
using Orchard.Localization;
using Orchard.Mvc;
using Orchard.Mvc.Extensions;
using Orchard.Security;
using Orchard.UI.Admin;
using Orchard.UI.Notify;
using Proligence.Collections.Services;

namespace Proligence.Collections.Controllers
{
    [Admin, ValidateInput(false)]
    public class AdminController : Controller
    {
        private readonly IAuthorizer _authorizer;
        private readonly INotifier _notifier;
        private readonly ICollectionService _collectionService;
        private readonly IOrchardServices _services;

        public AdminController(IOrchardServices services, IAuthorizer authorizer, INotifier notifier, ICollectionService collectionService)
        {
            _authorizer = authorizer;
            _notifier = notifier;
            _collectionService = collectionService;
            _services = services;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        [HttpPost]
        public ActionResult Add(int collectionId, int itemId, string returnUrl)
        {
            if (!_authorizer.Authorize(StandardPermissions.SiteOwner)) return new HttpUnauthorizedResult();
            var item = _services.ContentManager.Get(itemId);

            try
            {
                _collectionService.AddItem(item, collectionId);
                _notifier.Information(T("Item added to the collection successfully."));
            }
            catch (Exception ex)
            {
                _notifier.Error(T("Could not add item to the collection. Reason: {0}", ex.Message));
            }
            return this.RedirectLocal(returnUrl);
        }

        [HttpPost]
        public ActionResult Remove(int recordId, string returnUrl)
        {
            if (!_authorizer.Authorize(StandardPermissions.SiteOwner)) return new HttpUnauthorizedResult();

            try
            {
                _collectionService.RemoveItem(recordId);
                _notifier.Information(T("Item removed from the collection successfully."));
            }
            catch (Exception ex)
            {
                _notifier.Error(T("Could not remove item from the collection. Reason: {0}", ex.Message));
            }

            return this.RedirectLocal(returnUrl);
        }

        [HttpPost]
        public ActionResult MoveUp(int recordId, string returnUrl)
        {
            if (!_authorizer.Authorize(StandardPermissions.SiteOwner)) return new HttpUnauthorizedResult();
            if (!_collectionService.MoveUp(recordId)) _notifier.Information(T("Cannot move item up."));
            return this.RedirectLocal(returnUrl);

        }

        [HttpPost]
        public ActionResult MoveDown(int recordId, string returnUrl)
        {
            if (!_authorizer.Authorize(StandardPermissions.SiteOwner)) return new HttpUnauthorizedResult();
            if (!_collectionService.MoveDown(recordId)) _notifier.Information(T("Cannot move item down."));
            return this.RedirectLocal(returnUrl);

        }
    }
}
