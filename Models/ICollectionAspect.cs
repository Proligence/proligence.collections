using System.Collections.Generic;
using Orchard.ContentManagement;

namespace Proligence.Collections.Models
{
    /// <summary>
    /// Describes an item that contains other items.
    /// </summary>
    public interface ICollectionAspect : IContent
    {
        /// <summary>
        /// List of contained items.
        /// </summary>
        IEnumerable<IContent> Items { get; }
    }
}