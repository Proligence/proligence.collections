﻿using Orchard.ContentManagement.Records;

namespace Proligence.Collections.Models
{
    /// <summary>
    /// n-m relation record (switch table) between Collections and products.
    /// </summary>
    public class CollectionRelationRecord
    {
            public virtual int Id { get; set; }
            public virtual CollectionPartRecord CollectionPartRecord { get; set; }
            public virtual ContentItemRecord ContentItemRecord { get; set; }
            public virtual string Position { get; set; }
    }
}