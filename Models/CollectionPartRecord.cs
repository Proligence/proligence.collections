using System.Collections.Generic;
using Orchard.ContentManagement.Records;
using Orchard.Data.Conventions;

namespace Proligence.Collections.Models {
	
    public class CollectionPartRecord : ContentPartRecord {
        public CollectionPartRecord() {
             Items = new List<CollectionRelationRecord>(); 
        }

        [CascadeAllDeleteOrphan]
        public virtual IList<CollectionRelationRecord> Items { get; set; }
    }
}