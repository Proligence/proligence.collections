using System;
using System.Collections.Generic;
using System.Linq;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Utilities;
using Orchard.UI;
using Proligence.Collections.Services;
using Proligence.Collections.Settings;

namespace Proligence.Collections.Models {
    /// <summary>
    /// Describes a Collection, which can contain multiple items inside (as an n-m relation).
    /// This part implements IList interface, so the underlying collection can be operated on without direct use of ICollectionService.
    /// </summary>
    public class CollectionPart : ContentPart<CollectionPartRecord>, ICollectionAspect {
        private readonly LazyField<IList<Tuple<CollectionRelationRecord, IContent>>> _items = new LazyField<IList<Tuple<CollectionRelationRecord, IContent>>>();

        public ICollectionService CollectionService { get; set; }

        public LazyField<IList<Tuple<CollectionRelationRecord, IContent>>> ItemsField {
            get { return _items; }
        }

        /// <summary>
        /// All collection items with corresponding relation record
        /// </summary>
        public IList<Tuple<CollectionRelationRecord, IContent>> AllItems {
            get { return _items.Value; }
            set { _items.Value = value; }
        }

        public IEnumerable<IContent> AvailableItems { get; set; }

        /// <summary>
        /// Forces reloading of items held by this CollectionPart. 
        /// Should be called after performing CRUD list operations on this CollectionPart.
        /// </summary>
        public void ForceRefresh()
        {
            // forces call to setter to refresh items.
            ItemsField.Value = null;
        }

        public bool AllowAdding {
            get {
                var set = Settings.GetModel<CollectionSettings>();
                return set.AllowAdding;
            }
        }

        public bool AllowRemoving {
            get {
                var set = Settings.GetModel<CollectionSettings>();
                return set.AllowRemoving;
            }
        }

        public IEnumerable<IContent> Items
        {
            get
            {
                if (AllItems == null)
                {
                    return Enumerable.Empty<IContent>();
                }

                return AllItems
                    .OrderBy(t => t.Item1.Position, new FlatPositionComparer())
                    .Select(t => t.Item2);
            }
        }
    }
}