using System;
using System.Linq;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Orchard.UI;
using Proligence.Collections.Models;
using Proligence.Collections.Services;

namespace Proligence.Collections.Handlers
{
    /// <summary>
    /// Default handler for Collection part.
    /// </summary>
    public class CollectionPartHandler : ContentHandler
    {
        private readonly IContentManager _contentManager;

        public CollectionPartHandler(
            IRepository<CollectionPartRecord> repository,
            IRepository<CollectionRelationRecord> repositoryRelation,
            IContentManager contentManager, ICollectionService collectionService)
        {
            _contentManager = contentManager;
            Filters.Add(StorageFilter.For(repository));

            // Setting up lazy load handlers for contained item list
            OnLoading<CollectionPart>(LazyLoadHandlers);

            // Clearing switch table records when Collection item gets removed.
            OnRemoved<CollectiblePart>((ctx, part) =>
            {
                foreach (var item in repositoryRelation.Fetch(i => i.ContentItemRecord.Id == part.Id))
                {
                    repositoryRelation.Delete(item);
                }
            });

            OnActivated<CollectionPart>((ctx, part) => part.CollectionService = collectionService);
        }

        protected void LazyLoadHandlers(LoadContentContext context, CollectionPart part)
        {
            // add handlers that will load content for id's just-in-time
            part.ItemsField.Loader(list => part.Record.Items.ToList().OrderBy(r => r.Position, new FlatPositionComparer())
                                               .Select(i => new Tuple<CollectionRelationRecord, IContent>(i, _contentManager.Get(i.ContentItemRecord.Id)))
                                               .ToList());

            // setting setter to force reloading items whenever SetValue gets called on the collection
            part.ItemsField.Setter(list => part.Record.Items.ToList().OrderBy(r => r.Position, new FlatPositionComparer())
                                               .Select(i => new Tuple<CollectionRelationRecord, IContent>(i, _contentManager.Get(i.ContentItemRecord.Id)))
                                               .ToList());
        }
    }
}