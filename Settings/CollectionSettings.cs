﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.ContentManagement.ViewModels;
using Orchard.Localization;

namespace Proligence.Collections.Settings
{
    /// <summary>
    /// Content type settings for SubMenu part
    /// </summary>
    public class CollectionSettings {
        private string _ofType = "";
        private string _ofPart = "CollectiblePart";
        private bool _allowAdding = true;
        private bool _allowRemoving = true;

        /// <summary>
        /// Type that collection type holds.
        /// </summary>
        public string OfType
        {
            get { return _ofType; }
            set { _ofType = value; }
        }

        /// <summary>
        /// Part name that collection type hold.
        /// </summary>
        public string OfPart
        {
            get { return _ofPart; }
            set { _ofPart = value; }
        }

        /// <summary>
        /// Allow adding items to the collection
        /// </summary>
        public bool AllowAdding
        {
            get { return _allowAdding; }
            set { _allowAdding = value; }
        }

        /// <summary>
        /// Allow removing items from the collection.
        /// </summary>
        public bool AllowRemoving
        {
            get { return _allowRemoving; }
            set { _allowRemoving = value; }
        }

        public SelectList AvailableParts { get; set; }
        public SelectList AvailableTypes { get; set; } 
    }

    /// <summary>
    /// Overrides default editors to enable putting settings on SubMenu part.
    /// </summary>
    public class CollectionSettingsHooks : ContentDefinitionEditorEventsBase
    {
        private readonly IContentDefinitionManager _definitions;

        public CollectionSettingsHooks(IContentDefinitionManager definitions) {
            _definitions = definitions;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        /// <summary>
        /// Overrides editor shown when part is attached to content type. Enables adding setting field to the content part
        /// attached.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns></returns>
        public override IEnumerable<TemplateViewModel> TypePartEditor(ContentTypePartDefinition definition)
        {
            if (definition.PartDefinition.Name != "CollectionPart")
                yield break;
            var model = definition.Settings.GetModel<CollectionSettings>();
            AttachLists(model);
            yield return DefinitionTemplate(model);
        }

        public override IEnumerable<TemplateViewModel> TypePartEditorUpdate(ContentTypePartDefinitionBuilder builder, IUpdateModel updateModel)
        {
            if (builder.Name != "CollectionPart")
                yield break;

            var model = new CollectionSettings();

            updateModel.TryUpdateModel(model, "CollectionSettings", null, null);
            builder.WithSetting("CollectionSettings.OfPart", model.OfPart);
            builder.WithSetting("CollectionSettings.OfType", model.OfType);
            builder.WithSetting("CollectionSettings.AllowAdding", model.AllowAdding.ToString());
            builder.WithSetting("CollectionSettings.AllowRemoving", model.AllowRemoving.ToString());
            AttachLists(model);
            yield return DefinitionTemplate(model);
        }

        private void AttachLists(CollectionSettings model) {
            var collTypes = _definitions.ListTypeDefinitions();
            var collParts = _definitions.ListPartDefinitions();

            var typeItems = new[] { new SelectListItem { Text = T("(All)").Text, Value = "" } }
                .Concat(collTypes.Where(t => t.Parts.Any(p => p.PartDefinition.Name == "CollectiblePart")).Select(x => new SelectListItem
                {
                    Value = x.Name,
                    Text = x.DisplayName,
                    Selected = x.Name == model.OfType,
                })).ToList();

            var partItems = new[] { new SelectListItem { Text = T("(All)").Text, Value = "" } }
                .Concat(collParts.Select(x => new SelectListItem
                {
                    Value = x.Name,
                    Text = x.Name,
                    Selected = x.Name == model.OfType,
                })).ToList();

            model.AvailableTypes = new SelectList(typeItems, "Value", "Text", model.OfType);
            model.AvailableParts = new SelectList(partItems, "Value", "Text", model.OfPart);
        }

    }
}
