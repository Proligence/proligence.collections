using System.Linq;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.Localization;
using Orchard.UI.Notify;
using Proligence.Collections.Models;
using Proligence.Collections.Settings;

namespace Proligence.Collections.Drivers {
    /// <summary>
    /// Content part driver for displaying/editing a Collection part
    /// </summary>
    public class CollectionPartDriver : ContentPartDriver<CollectionPart> {
        private const string TemplateName = "Parts/Common.Collection";
        private readonly INotifier _notifier;
        private readonly IContentManager _content;
        private readonly IContentDefinitionManager _definition;

        public CollectionPartDriver(INotifier notifier, IContentManager content, IContentDefinitionManager definition) {
            _notifier = notifier;
            _content = content;
            _definition = definition;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override DriverResult Editor(CollectionPart part, dynamic shapeHelper) {
            var settings = part.Settings.GetModel<CollectionSettings>();

            var types = _definition
                .ListTypeDefinitions()
                .Where(t => (string.IsNullOrEmpty(settings.OfType) || t.Name == settings.OfType) 
                    && t.Parts.Any(p => string.IsNullOrEmpty(settings.OfPart) || (p.PartDefinition.Name == settings.OfPart))).Select(d => d.Name).ToList();

            var collectibles = _content
                .Query(VersionOptions.Published)
                .ForType(types.ToArray())
                .List();

            part.AvailableItems = collectibles.Where(p => types.Contains(p.ContentType)).ToList();
            return Editor(part, (IUpdateModel)null, shapeHelper);
        }

        protected override DriverResult Editor(CollectionPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var editor = shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix);
            return ContentShape(
                "Parts_Common_Collection",
                () => editor);
        }
    }
}