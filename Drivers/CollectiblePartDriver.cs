using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;
using Orchard.UI.Notify;
using Proligence.Collections.Models;

namespace Proligence.Collections.Drivers
{
    public class CollectiblePartDriver : ContentPartDriver<CollectiblePart>
    {
        private readonly INotifier _notifier;
        private const string TemplateName = "Parts/Common.Collectible";

        public Localizer T { get; set; }

        protected override string Prefix
        {
            get { return "Collectible"; }
        }

        public CollectiblePartDriver(INotifier notifier)
        {
            _notifier = notifier;
            T = NullLocalizer.Instance;
        }

        protected override DriverResult Editor(CollectiblePart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_Common_Collectible",
                    () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(CollectiblePart part, IUpdateModel updater, dynamic shapeHelper)
        {
            if (updater.TryUpdateModel(part, Prefix, null, null)) {
                _notifier.Information(T("Collectible part edited successfully"));
            }
            else
            {
                _notifier.Error(T("Error during Collectible part update!"));
            }
            return Editor(part, shapeHelper);
        }

    }
}