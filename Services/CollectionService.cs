﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.UI;
using Proligence.Collections.Models;
using Proligence.Collections.Settings;

namespace Proligence.Collections.Services
{
    public class CollectionService : ICollectionService
    {
        private readonly IOrchardServices _services;
        private readonly IRepository<CollectionRelationRecord> _repository;

        public CollectionService(IOrchardServices services, IRepository<CollectionRelationRecord> repository)
        {
            _services = services;
            _repository = repository;
        }

        public void ClearCollection(int collectionId)
        {
            var items = _repository.Fetch(i => i.CollectionPartRecord.Id == collectionId);
            if (items != null)
                foreach (var item in items)
                    _repository.Delete(item);
        }

        private CollectionRelationRecord GetRecord(int recordId)
        {
            return _repository.Get(recordId);
        }

        private IEnumerable<CollectionRelationRecord> GetRecordsForCollection(int collectionId)
        {
            return _repository.Fetch(r => r.CollectionPartRecord.Id == collectionId);
        }

        public void RemoveItem(int recordId)
        {
            var item = _repository.Get(recordId);
            if (item == null) throw new ArgumentException("Item with specified Id does not exist.", "recordId");

            var col = _services.ContentManager.Get<CollectionPart>(item.CollectionPartRecord.Id);
            if (!col.Settings.GetModel<CollectionSettings>().AllowRemoving) throw new InvalidOperationException("Collection doesn't allow item removal.");

            _repository.Delete(item);
            _repository.Flush();
        }

        public void AddItem<T>(T item, int collectionId) where T : class, IContent
        {
            if (item == null)
            {
                throw new ArgumentException("Item cannot be null.", "item");
            }

            var col = _services.ContentManager.Get<CollectionPart>(collectionId);
            if (col == null)
            {
                throw new ArgumentException("Specified collection does not exist.", "collectionId");
            }

            if (!col.Settings.GetModel<CollectionSettings>().AllowAdding)
            {
                throw new InvalidOperationException("Collection doesn't allow adding items.");
            }

            var lastPosition = GetRecordsForCollection(collectionId)
                .OrderByDescending(r => r.Position, new FlatPositionComparer())
                .Select(ParsePosition)
                .FirstOrDefault();

            _repository.Create(new CollectionRelationRecord { CollectionPartRecord = col.Record, ContentItemRecord = item.ContentItem.Record, Position = (lastPosition + 1).ToString() });
            _repository.Flush();
        }

        public bool MoveDown(int recordId)
        {
            var record = GetRecord(recordId);
            int currentPosition = ParsePosition(record);

            var recordAfter = GetRecordsForCollection(record.CollectionPartRecord.Id)
                .OrderByDescending(r => r.Position, new FlatPositionComparer())
                .FirstOrDefault(r => ParsePosition(r) > currentPosition);

            if (recordAfter != null)
            {
                recordAfter.Position = record.Position;
                MakeRoomForPosition(recordAfter);
                _repository.Flush();
                return true;
            }

            return false;
        }

        public bool Contains(int collectionId, int itemId) {
            return _repository.Count(r => r.CollectionPartRecord.Id == collectionId && r.ContentItemRecord.Id == itemId) > 0;
        }

        public bool RemoveFirst(int collectionId, int itemId) {
            var item = _repository.Get(r => r.CollectionPartRecord.Id == collectionId && r.ContentItemRecord.Id == itemId);
            if (item == null) return false;

            try {
                _repository.Delete(item);
                _repository.Flush();
            }
            catch(Exception ex) {
                return false;
            }

            return true;
        }

        public bool MoveUp(int recordId)
        {
            var record = GetRecord(recordId);
            int currentPosition = ParsePosition(record);

            var recordBefore = GetRecordsForCollection(record.CollectionPartRecord.Id)
                .OrderByDescending(r => r.Position, new FlatPositionComparer())
                .FirstOrDefault(r => ParsePosition(r) < currentPosition);

            if (recordBefore != null)
            {
                record.Position = recordBefore.Position;
                MakeRoomForPosition(record);
                _repository.Flush();
                return true;
            }

            return false;
        }

        private void MakeRoomForPosition(CollectionRelationRecord relation)
        {
            int targetPosition = ParsePosition(relation);

            var recordsToMove = GetRecordsForCollection(relation.CollectionPartRecord.Id)
                .Where(r => ParsePosition(r) >= targetPosition && r.Id != relation.Id)
                .OrderBy(r => r.Position, new FlatPositionComparer()).ToList();

            // no need to continue if there are no widgets that will conflict with this widget's position
            if (!recordsToMove.Any() || ParsePosition(recordsToMove.First()) > targetPosition)
                return;

            int position = targetPosition;
            foreach (var record in recordsToMove)
                record.Position = (++position).ToString();
        }

        public void MakeRoomFor(int targetPosition, int collectionId) {

            var recordsToMove = GetRecordsForCollection(collectionId)
                .Where(r => ParsePosition(r) >= targetPosition)
                .OrderBy(r => r.Position, new FlatPositionComparer()).ToList();

            if (!recordsToMove.Any() || ParsePosition(recordsToMove.First()) > targetPosition)
                return;

            int position = targetPosition;
            foreach (var record in recordsToMove)
                record.Position = (++position).ToString();
        }

        private static int ParsePosition(CollectionRelationRecord relation)
        {
            int value;
            if (!int.TryParse(relation.Position, out value))
                return 0;
            return value;
        }


    }
}