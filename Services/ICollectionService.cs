using Orchard;
using Orchard.ContentManagement;

namespace Proligence.Collections.Services {
    public interface ICollectionService : IDependency {
        void ClearCollection(int collectionId);
        void RemoveItem(int recordId);
        void AddItem<T>(T item, int collectionId) where T : class, IContent;
        bool MoveUp(int recordId);
        bool MoveDown(int recordIdd);
        bool Contains(int collectionId, int itemId);
        bool RemoveFirst(int itemId, int collectionId);
        void MakeRoomFor(int targetPosition, int collectionId);
    }
}